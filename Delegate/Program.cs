﻿using System;
using ClassLib.Common.Delegates.Tasks;
using ClassLib.Core.Interfaces;

namespace Delegate
{
    internal static class Program
    {
        private static void Main()
        {
            IRunnable[] tasks = {new First(), new Second()};
            foreach (var runnable in tasks) runnable.Run();
            Console.ReadKey();
        }
    }
}
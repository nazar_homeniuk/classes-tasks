# EPAM .NET Classes Tasks

## SonarCloud report

To view the code coverage, code smells, bugs, vulnerabilities, dublications visit [SonarCloud](https://sonarcloud.io/dashboard?id=ClassesTasks).

**Some code smells are forced in connection with the requirements of the task.**

## Author

* **Nazar Homeniuk** - *Facebook* - [Nazar Homeniuk](https://www.facebook.com/nazar.homenyuk)
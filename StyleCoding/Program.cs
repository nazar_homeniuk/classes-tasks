﻿using System;
using ClassLib.Common.StyleCoding;
using StyleCoding.Builders;
using StyleCoding.Controlers;

namespace StyleCoding
{
    public static class Program
    {
        public static void Main()
        {
            var builder = new PictureBuilder();
            var printer = new ConsolePrinter();
            Rect first;
            Rect second;
            Rect result;
            string choose;
            while (true)
            {
                Console.Clear();
                Console.WriteLine("1.Create one rectangle\n2.Intersect\n3.Union\n4.Create circle");
                choose = Console.ReadLine();
                switch (choose)
                {
                    case "1":
                        var rect = ShapeBuilder.BuildRect();
                        var rectControler = new ConsoleControler<Rect>(rect);
                        printer.Print(builder.BuildRect(Console.WindowHeight, Console.WindowWidth, rect));
                        while (rectControler.Move(Console.ReadKey().Key))
                            printer.Print(builder.BuildRect(Console.WindowHeight, Console.WindowWidth, rect));

                        break;
                    case "2":
                        first = ShapeBuilder.BuildRect();
                        second = ShapeBuilder.BuildRect();
                        result = Rect.Intersect(first, second);
                        if (result == null)
                        {
                            Console.Clear();
                            Console.WriteLine("There are no intersection!");
                            Console.ReadKey();
                            break;
                        }

                        rectControler = new ConsoleControler<Rect>(result);
                        printer.Print(builder.BuildRect(Console.WindowHeight, Console.WindowWidth, result));
                        while (rectControler.Move(Console.ReadKey().Key))
                            printer.Print(builder.BuildRect(Console.WindowHeight, Console.WindowWidth, result));

                        break;
                    case "3":
                        first = ShapeBuilder.BuildRect();
                        second = ShapeBuilder.BuildRect();
                        result = Rect.Union(first, second);
                        rectControler = new ConsoleControler<Rect>(result);
                        printer.Print(builder.BuildRect(Console.WindowHeight, Console.WindowWidth, result));
                        while (rectControler.Move(Console.ReadKey().Key))
                            printer.Print(builder.BuildRect(Console.WindowHeight, Console.WindowWidth, result));

                        break;
                    case "4":
                        var circ = ShapeBuilder.BuildCircle();
                        var circleControler = new ConsoleControler<Circle>(circ);
                        printer.Print(builder.BuildCircle(Console.WindowHeight, Console.WindowWidth, circ));
                        while (circleControler.Move(Console.ReadKey().Key))
                            printer.Print(builder.BuildCircle(Console.WindowHeight, Console.WindowWidth, circ));

                        Console.ReadKey();
                        break;
                    default:
                        continue;
                }
            }
        }
    }
}
﻿using System;
using ClassLib.Common.StyleCoding;

namespace StyleCoding.Builders
{
    public class PictureBuilder
    {
        public bool[,] BuildRect(int height, int width, Rect rect)
        {
            var result = new bool[height, width];
            for (var i = 0; i < height; ++i)
            {
                for (var j = 0; j < width; ++j)
                {
                    if (i >= height - rect.TopLeft.Y && i <= height - rect.BottomRight.Y)
                    {
                        if (j >= rect.TopLeft.X && j <= rect.BottomRight.X) result[i, j] = true;
                    }
                    else
                    {
                        result[i, j] = false;
                    }
                }
            }

            return result;
        }

        public bool[,] BuildCircle(int height, int width, Circle circ)
        {
            var result = new bool[height, width];
            for (var i = 0; i < height; ++i)
            {
                for (var j = 0; j < width; ++j)
                {
                    if (Math.Round(Math.Sqrt(Math.Pow(circ.Center.X - j, 2) + Math.Pow(circ.Center.Y - i, 2))) <
                        circ.Radius)
                        result[i, j] = true;
                    else
                        result[i, j] = false;
                }
            }

            return result;
        }
    }
}
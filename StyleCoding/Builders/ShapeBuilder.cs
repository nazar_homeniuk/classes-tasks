﻿using System;
using ClassLib.Common.StyleCoding;

namespace StyleCoding.Builders
{
    public static class ShapeBuilder
    {
        public static Rect BuildRect()
        {
            int leftX;
            int leftY;
            int rightX;
            int rightY;
            Console.Clear();
            Console.WriteLine("Enter the bottom left point");
            Console.Write("X:");
            leftX = Convert.ToInt32(Console.ReadLine());
            Console.Write("Y:");
            leftY = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the top right point");
            Console.Write("X:");
            rightX = Convert.ToInt32(Console.ReadLine());
            Console.Write("Y:");
            rightY = Convert.ToInt32(Console.ReadLine());
            return new Rect(leftX, leftY, rightX, rightY);
        }

        public static Circle BuildCircle()
        {
            int centerX;
            int centerY;
            int radius;
            Console.Clear();
            Console.WriteLine("Enter the center point");
            Console.Write("X:");
            centerX = Convert.ToInt32(Console.ReadLine());
            Console.Write("Y:");
            centerY = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter the radius:");
            radius = Convert.ToInt32(Console.ReadLine());
            return new Circle(radius, centerX, centerY);
        }
    }
}
﻿using System;
using System.Text;
using ClassLib.Common.StyleCoding.Interfaces;

namespace StyleCoding
{
    public class ConsolePrinter : IPrinter
    {
        public void Print(bool[,] arr)
        {
            Console.Clear();
            var s = new StringBuilder();
            for (var i = arr.GetLength(0) - 1; i >= 0; --i)
            {
                if (i == arr.GetLength(0) - 1)
                    s.Append("^");
                else if (i == 0)
                    s.Append("╚");
                else
                    s.Append("╫");

                for (var j = 0; j < arr.GetLength(1); ++j)
                    if (arr[i, j])
                        s.Append("#");
                    else if (i == 0)
                        s.Append("╧");
                    else if (j == 0 && i == 0)
                        s.Append(">");
                    else
                        s.Append(" ");

                s.Append("\n");
            }

            Console.Write(s.ToString());
        }
    }
}
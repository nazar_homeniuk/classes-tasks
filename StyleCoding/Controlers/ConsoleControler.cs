﻿using System;
using ClassLib.Common.StyleCoding.Interfaces;

namespace StyleCoding.Controlers
{
    public class ConsoleControler<T> where T : IMoveble, ISizeble
    {
        public ConsoleControler(T movebleObject)
        {
            MovebleObject = movebleObject;
        }

        public T MovebleObject { get; }

        public bool Move(ConsoleKey key)
        {
            switch (key)
            {
                case ConsoleKey.LeftArrow:
                    MovebleObject.MoveLeft();
                    return true;
                case ConsoleKey.RightArrow:
                    MovebleObject.MoveRight();
                    return true;
                case ConsoleKey.UpArrow:
                    MovebleObject.MoveUp();
                    return true;
                case ConsoleKey.DownArrow:
                    MovebleObject.MoveDown();
                    return true;
                case ConsoleKey.OemPlus:
                    MovebleObject.ZoomIn();
                    return true;
                case ConsoleKey.OemMinus:
                    MovebleObject.ZoomOut();
                    return true;
                default:
                    return false;
            }
        }
    }
}
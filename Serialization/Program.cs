﻿using ClassLib.Common.Serialization.Tasks;
using ClassLib.Core.Interfaces;

namespace Serialization
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            IRunnable task = new First();
            task.Run();
        }
    }
}
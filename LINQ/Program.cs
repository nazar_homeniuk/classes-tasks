﻿using ClassLib.Common.LINQ.Tasks;
using ClassLib.Core.Interfaces;

namespace LINQ
{
    internal static class Program
    {
        private static void Main()
        {
            IRunnable[] tasks = {new FirstAndSecond(), new Fourth()};
            foreach (var i in tasks) i.Run();
        }
    }
}
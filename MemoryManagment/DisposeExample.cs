﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;

namespace MemoryManagment
{
    public class DisposeExample : IDisposable
    {
        private readonly SafeHandle _resource; // disposable handle to a resource  

        private readonly Stopwatch _sw;

        public DisposeExample()
        {
            var buffer = new IntPtr();
            _resource = new SafeFileHandle(buffer, true); // allocates the resource  
            _sw = Stopwatch.StartNew();
            Console.WriteLine("Instantiated object");
        }

        private bool _disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            Console.WriteLine($"Dispose with param {disposing} time:{_sw.Elapsed}");
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _sw?.Stop();
                    _resource?.Dispose();
                }

                _disposedValue = true;
            }
        }

        ~DisposeExample()
        {
            Console.WriteLine($"Finalizing object by finalizator time:{_sw.Elapsed}");
            Dispose(false);
        }

        void IDisposable.Dispose()
        {
            Console.WriteLine($"Finalizing object by Dispose time:{_sw.Elapsed}");
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
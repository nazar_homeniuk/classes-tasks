﻿using System;
using ClassLib.Common.Exceptions.Tasks;
using ClassLib.Core.Interfaces;

namespace Exceptions
{
    internal static class Program
    {
        private static void Main()
        {
            IRunnable[] tasks = {new First(), new Second(), new Fifth()};
            foreach (var runnable in tasks) runnable.Run();

            Console.ReadKey();
        }
    }
}
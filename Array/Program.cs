﻿using System;
using ClassLib.Common.Array;
using ClassLib.Common.Array.Tasks;
using ClassLib.Core.Interfaces;

namespace Array
{
    internal static class Program
    {
        private static void Main()
        {
            IRunnable[] tasks =
            {
                new First(new ConsoleReader(), new ConsolePrinter()),
                new Second(new ConsoleReader(), new ConsolePrinter()),
                new Third(new ConsoleReader(), new ConsolePrinter()),
                new Fourth(new ConsoleReader(), new ConsolePrinter()),
                new Fifth(new ConsoleReader(), new ConsolePrinter())
            };
            foreach (var i in tasks)
            {
                Console.Clear();
                i.Run();
                Console.ReadKey();
            }
        }
    }
}
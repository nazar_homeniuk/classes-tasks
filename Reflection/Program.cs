﻿using System;
using ClassLib.Common.Reflection.Tasks;
using ClassLib.Core.Interfaces;

namespace Reflection
{
    internal static class Program
    {
        private static void Main()
        {
            IRunnable[] tasks = {new First()};
            foreach (var runnable in tasks) runnable.Run();
            Console.ReadKey();
        }
    }
}
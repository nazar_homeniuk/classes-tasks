﻿using System;
using ClassLib.Common.Introduction.Tasks;
using ClassLib.Core.Interfaces;

namespace ClassesTasks
{
    internal static class Program
    {
        private static void Main()
        {
            IRunnable[] tasks = {new First(), new Second()};
            foreach (var runnable in tasks)
            {
                Console.Clear();
                runnable.Run();
                Console.ReadKey();
            }
        }
    }
}
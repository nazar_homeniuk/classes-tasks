﻿using System;
using ClassLib.Common.Vector;

namespace Vector
{
    internal static  class Program
    {
        private static void Main()
        {
            ClassLib.Common.Vector.Vector first;
            ClassLib.Common.Vector.Vector second;
            ClassLib.Common.Vector.Vector third;
            while (true)
            {
                Console.Clear();
                Console.WriteLine(
                    "1.Add\n2.Sub\n3.Scalar product\n4.Cross product\n5.Mixed product\n6.Vector lenght\n7.Angle\n8.Compare\n9.Exit");
                var choose = Console.ReadLine();
                switch (choose)
                {
                    case "1":
                        first = VectorCreator.CreateVector();
                        second = VectorCreator.CreateVector();
                        Console.Clear();
                        Console.WriteLine(first + second);
                        Console.ReadKey();
                        break;
                    case "2":
                        first = VectorCreator.CreateVector();
                        second = VectorCreator.CreateVector();
                        Console.Clear();
                        Console.WriteLine(first - second);
                        Console.ReadKey();
                        break;
                    case "3":
                        first = VectorCreator.CreateVector();
                        second = VectorCreator.CreateVector();
                        Console.Clear();
                        Console.WriteLine(ClassLib.Common.Vector.Vector.ScalarProd(first, second));
                        Console.ReadKey();
                        break;
                    case "4":
                        first = VectorCreator.CreateVector();
                        second = VectorCreator.CreateVector();
                        Console.Clear();
                        Console.WriteLine(ClassLib.Common.Vector.Vector.CrossProd(first, second));
                        break;
                    case "5":
                        first = VectorCreator.CreateVector();
                        second = VectorCreator.CreateVector();
                        third = VectorCreator.CreateVector();
                        Console.Clear();
                        Console.WriteLine(ClassLib.Common.Vector.Vector.MixedProd(first, second, third));
                        Console.ReadKey();
                        break;
                    case "6":
                        first = VectorCreator.CreateVector();
                        Console.Clear();
                        Console.WriteLine(first + "\nLenght: " + first.Abs());
                        Console.ReadKey();
                        break;
                    case "7":
                        first = VectorCreator.CreateVector();
                        second = VectorCreator.CreateVector();
                        Console.Clear();
                        Console.WriteLine("Sin: {0}\nCos: {1}", ClassLib.Common.Vector.Vector.Sin(first, second), ClassLib.Common.Vector.Vector.Cos(first, second));
                        Console.ReadKey();
                        break;
                    case "8":
                        first = VectorCreator.CreateVector();
                        second = VectorCreator.CreateVector();
                        Console.Clear();
                        Console.WriteLine("<: {0}\n>: {1}\n==: {2}\n!=: {3}", (first < second).ToString(),
                            (first > second).ToString(), (first == second).ToString(), (first != second).ToString());
                        Console.ReadKey();
                        break;
                    case "9":
                        Environment.Exit(0);
                        break;
                    default:
                        continue;
                }
            }
        }
    }
}
﻿using System;
using ClassLib.Common.Collections.Tasks;
using ClassLib.Core.Interfaces;

namespace Collections
{
    internal static class Program
    {
        private static void Main()
        {
            IRunnable[] tasks = {new FirstAndSecond(), new Third()};
            foreach (var i in tasks) i.Run();

            Console.ReadKey();
        }
    }
}
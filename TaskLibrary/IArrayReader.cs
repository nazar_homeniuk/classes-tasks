﻿namespace TaskLibrary
{
    public interface IArrayReader<T>
    {
        T[] ReadOneDim();
        T[,] ReadTwoDim();
    }
}

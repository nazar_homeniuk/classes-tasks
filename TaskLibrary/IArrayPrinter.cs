﻿using System.Collections.Generic;

namespace TaskLibrary
{
    public interface IArrayPrinter<T>
    {
        void Print(T[] arr);
        void Print(T[,] arr);
    }
}

﻿namespace TaskLibrary
{
    public interface ITask
    {
        void Solve();
    }
}

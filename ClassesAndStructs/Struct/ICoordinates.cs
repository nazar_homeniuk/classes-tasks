﻿using System;

namespace ClassTasks.Struct
{
    internal interface ICoordinates
    {
        Tuple<double, double> LeftTop { get; }

        Tuple<double, double> RightBot { get; }
    }
}
﻿using System;

namespace ClassTasks.Struct
{
    internal struct Rectangle : ICoordinates, ISize
    {
        public double Width { get; }
        public double Height { get; }
        public Tuple<double, double> LeftTop { get; }
        public Tuple<double, double> RightBot { get; }

        public Rectangle(Tuple<double, double> leftTop, Tuple<double, double> rightBot)
        {
            LeftTop = leftTop;
            RightBot = rightBot;
            Width = rightBot.Item1 - leftTop.Item1;
            Height = LeftTop.Item2 - rightBot.Item2;
        }

        public double Perimeter()
        {
            return Math.Abs(2 * Height) + Math.Abs(2 * Width);
        }
    }
}
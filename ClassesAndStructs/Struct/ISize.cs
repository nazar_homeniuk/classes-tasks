﻿namespace ClassTasks.Struct
{
    internal interface ISize
    {
        double Width { get; }

        double Height { get; }

        double Perimeter();
    }
}
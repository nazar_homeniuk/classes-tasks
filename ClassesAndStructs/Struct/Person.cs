﻿using System;

namespace ClassTasks.Struct
{
    internal struct Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }

        public Person(string firstname, string lastname, int age)
        {
            FirstName = firstname;
            LastName = lastname;
            Age = age;
        }

        public bool OlderOrYounger(int n)
        {
            if (n <= 0) return false;

            if (Age > n)
            {
                Console.WriteLine($"{FirstName} {LastName} older than {n}");
                return true;
            }

            if (Age < n)
            {
                Console.WriteLine($"{FirstName} {LastName} younger than {n}");
                return true;
            }

            Console.WriteLine($"{FirstName} {LastName} age equals to {n}");
            return true;
        }
    }
}
﻿using System;

namespace ClassTasks.EnumTask
{
    public static class Extentions
    {
        public static void Sort(this Colors colors)
        {
            var arr = Enum.GetValues(typeof(Colors));
            Array.Sort(arr);
            foreach (var i in arr) Console.WriteLine(Enum.GetName(typeof(Colors), i));
        }
    }

    public enum Months
    {
        January,
        February,
        March,
        April,
        May,
        June,
        July,
        August,
        September,
        October,
        November,
        December
    }

    public enum Colors
    {
        Red = 4,
        Green = 1,
        Blue = 15
    }

    public enum LongRange : long
    {
        Min = long.MinValue,
        Max = long.MaxValue
    }
}
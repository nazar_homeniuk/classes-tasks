﻿using System;

namespace ClassTasks.Class
{
    public class Rectangle
    {
        public Rectangle()
        {
            LeftUp = new Tuple<double, double>(0.0, 0.0);
            RightDown = new Tuple<double, double>(0.0, 0.0);
        }

        public Rectangle(Tuple<double, double> leftUp, Tuple<double, double> rightDown)
        {
            LeftUp = leftUp;
            RightDown = rightDown;
        }

        public Tuple<double, double> LeftUp { get; set; }

        public Tuple<double, double> RightDown { get; set; }

        public double Square()
        {
            return Math.Abs((RightDown.Item1 - LeftUp.Item1) * (LeftUp.Item2 - RightDown.Item2));
        }

        public double Perim()
        {
            return Math.Abs(2 * (RightDown.Item1 - LeftUp.Item1)) + Math.Abs(2 * (LeftUp.Item2 - RightDown.Item2));
        }
    }
}
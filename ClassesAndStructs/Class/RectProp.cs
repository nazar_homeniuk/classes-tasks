﻿using System;

namespace ClassTasks.Class
{
    public class RectProp
    {
        private double perim;
        private double square;

        public RectProp()
        {
            LeftUp = new Tuple<double, double>(0.0, 0.0);
            RightDown = new Tuple<double, double>(0.0, 0.0);
        }

        public RectProp(Tuple<double, double> leftUp, Tuple<double, double> rightDown)
        {
            LeftUp = leftUp;
            RightDown = rightDown;
        }

        public Tuple<double, double> LeftUp { get; set; }

        public Tuple<double, double> RightDown { get; set; }

        public double Square
        {
            get
            {
                square = Math.Abs((RightDown.Item1 - LeftUp.Item1) * (LeftUp.Item2 - RightDown.Item2));
                return square;
            }
            private set => square = value;
        }

        public double Perim
        {
            get
            {
                perim = Math.Abs(2 * (RightDown.Item1 - LeftUp.Item1)) + Math.Abs(2 * (LeftUp.Item2 - RightDown.Item2));
                return perim;
            }
            private set => perim = value;
        }
    }
}
﻿namespace ClassTasks.Class
{
    public class ComplexNumber
    {
        #region Overrided object methods

        public override string ToString()
        {
            return $"{Real} + {Imagine}i";
        }

        #endregion

        #region Fields

        public double Real { get; set; }

        public double Imagine { get; set; }

        #endregion

        #region Constructors

        public ComplexNumber()
        {
            Real = 0;
            Imagine = 0;
        }

        public ComplexNumber(double real, double imagine)
        {
            Real = real;
            Imagine = imagine;
        }

        #endregion

        #region Operatos

        public static ComplexNumber operator *(ComplexNumber first, ComplexNumber second)
        {
            return new ComplexNumber(first.Real * second.Real - first.Imagine * second.Imagine,
                first.Real * second.Imagine + first.Imagine * second.Real);
        }

        public static ComplexNumber operator /(ComplexNumber first, ComplexNumber second)
        {
            return new ComplexNumber(
                (first.Real * second.Real + first.Imagine * second.Imagine) /
                (second.Real * second.Real + second.Imagine * second.Imagine),
                (first.Imagine * second.Real - first.Real * second.Imagine) /
                (second.Real * second.Real + second.Imagine * second.Imagine));
        }

        #endregion
    }
}
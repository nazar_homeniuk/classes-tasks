﻿using System;

namespace ClassTasks.Class
{
    public static class RectangleStat
    {
        public static double Squre(double x1, double y1, double x2, double y2)
        {
            return Math.Abs((x2 - x1) * (y1 - y2));
        }

        public static double Perim(double x1, double y1, double x2, double y2)
        {
            return Math.Abs(2 * (x2 - x1)) + Math.Abs(2 * (y1 - y2));
        }
    }
}
﻿namespace ClassTasks.Class
{
    public static class CircleStat
    {
        private const double pi = 3.14;

        public static double Square(double radius)
        {
            return pi * radius * radius;
        }

        public static double Lenght(double radius)
        {
            return 2 * pi * radius;
        }
    }
}
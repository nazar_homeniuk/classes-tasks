﻿namespace ClassTasks.Class
{
    public class Circle
    {
        private const double pi = 3.14;

        public Circle()
        {
            Radius = 0;
        }

        public Circle(double radius)
        {
            Radius = radius;
        }

        public double Radius { get; set; }

        public double Lenght()
        {
            return 2 * pi * Radius;
        }

        public double Square()
        {
            return pi * Radius * Radius;
        }
    }
}
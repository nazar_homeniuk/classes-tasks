﻿using System;
using ClassTasks.Class;
using ClassTasks.EnumTask;
using ClassTasks.Struct;
using Rectangle = ClassTasks.Class.Rectangle;

namespace ClassTasks
{
    internal static class Program
    {
        private static void Main()
        {
            ClassTasks();
            StructTasks();
            EnumTasks();
        }

        private static void ClassTasks()
        {
            Console.Clear();
            Console.WriteLine("<----==== CLASS TASKS ====---->");
            Console.WriteLine("1. *****Rectangle******\nLeft up: ");
            Rectangle rect;
            double x1, x2, y1, y2;
            Console.WriteLine("Enter x: ");
            x1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter y: ");
            y1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Right down: ");
            Console.WriteLine("Enter x: ");
            x2 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter y: ");
            y2 = Convert.ToDouble(Console.ReadLine());
            rect = new Rectangle(new Tuple<double, double>(x1, y1), new Tuple<double, double>(x2, y2));
            Console.WriteLine("Square: {0}\nPerim: {1}", rect.Square(), rect.Perim());
            var rectProp = new RectProp(new Tuple<double, double>(x1, y1), new Tuple<double, double>(x2, y2));
            Console.WriteLine("2. *****Rectangle with propertyes*****\nSquare: {0}\nPerim: {1}", rectProp.Square,
                rectProp.Perim);
            Console.Write("3. Circle\nEnter the radius: ");
            double radius;
            radius = Convert.ToDouble(Console.ReadLine());
            var circle = new Circle(radius);
            Console.WriteLine("Square: {0}\nLenght: {1}", circle.Square(), circle.Lenght());
            Console.WriteLine("4. *****1 and 3 by static classes*****");
            Console.Write("Enter x: ");
            x1 = Convert.ToDouble(Console.ReadLine());
            Console.Write("Enter y: ");
            y1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Right down: ");
            Console.Write("Enter x: ");
            x2 = Convert.ToDouble(Console.ReadLine());
            Console.Write("Enter y: ");
            y2 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Square: {0}\nPerim: {1}", RectangleStat.Squre(x1, y1, x2, y2),
                RectangleStat.Perim(x1, y1, x2, y2));
            Console.Write("Enter the radius: ");
            radius = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Square: {0}\nLenght: {1}", CircleStat.Square(radius), CircleStat.Lenght(radius));
            Console.WriteLine("5. *****Complex number*****");
            double real1;
            double real2;
            double img1;
            double img2;
            Console.Write("Enter the first number\nReal:");
            real1 = Convert.ToDouble(Console.ReadLine());
            Console.Write("Imagine: ");
            img1 = Convert.ToDouble(Console.ReadLine());
            Console.Write("Enter the second number\nReal:");
            real2 = Convert.ToDouble(Console.ReadLine());
            Console.Write("Imagine: ");
            img2 = Convert.ToDouble(Console.ReadLine());
            var first = new ComplexNumber(real1, img1);
            var second = new ComplexNumber(real2, img2);
            Console.WriteLine("{0} * {1} = {2}\n{0} / {1} = {3}", first, second, first * second, first / second);
            Console.ReadKey();
        }

        private static void StructTasks()
        {
            Console.Clear();
            Console.WriteLine("<----==== STRUCT TASKS ====---->");
            string firstName;
            string lastName;
            int age;
            Console.WriteLine("1. *****Person struct*****");
            Console.Write("Enter first name: ");
            firstName = Console.ReadLine();
            Console.Write("Enter last name: ");
            lastName = Console.ReadLine();
            while (true)
            {
                Console.Write("Enter the age: ");
                age = Convert.ToInt32(Console.ReadLine());
                if (age <= 0) continue;
                break;
            }

            var person = new Person(firstName, lastName, age);
            int n;
            Console.Write("Enter the n: ");
            n = Convert.ToInt32(Console.ReadLine());
            while (!person.OlderOrYounger(n))
            {
                Console.Write("Enter the n: ");
                n = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("2. *****Rectangle interfaces*****");

            double x1, x2, y1, y2;
            Console.WriteLine("Enter x: ");
            x1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter y: ");
            y1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Right down: ");
            Console.WriteLine("Enter x: ");
            x2 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter y: ");
            y2 = Convert.ToDouble(Console.ReadLine());
            var rect = new Struct.Rectangle(new Tuple<double, double>(x1, y1), new Tuple<double, double>(x2, y2));
            Console.WriteLine("Perimeter: {0}", rect.Perimeter());
            Console.ReadKey();
        }

        private static void EnumTasks()
        {
            Console.Clear();
            Console.WriteLine("<----==== ENUM TASKS ====---->");
            Console.WriteLine("1. *****Months*****");
            int n;
            while (true)
            {
                Console.Write("Enter the n: ");
                n = Convert.ToInt32(Console.ReadLine());
                if (n < 0 || n >= 12) continue;
                break;
            }

            Console.WriteLine("Month: {0}", Enum.GetName(typeof(Months), n));
            Console.WriteLine("2. *****Colors*****");
            Colors.Blue.Sort();
            Console.WriteLine("3. *****Long range*****");
            Console.WriteLine("Min: {0}\nMax: {1}", (long) LongRange.Min, (long) LongRange.Max);
            Console.ReadKey();
        }
    }
}
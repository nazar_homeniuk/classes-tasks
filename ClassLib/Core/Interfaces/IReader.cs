﻿namespace ClassLib.Core.Interfaces
{
    public interface IReader
    {
        string ReadText();
    }
}
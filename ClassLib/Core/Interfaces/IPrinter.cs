﻿namespace ClassLib.Core.Interfaces
{
    public interface IPrinter
    {
        void PrintText(string text);
    }
}
﻿namespace ClassLib.Core.Interfaces
{
    public interface IRunnable
    {
        void Run();
    }
}
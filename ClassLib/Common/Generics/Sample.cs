﻿using System;
using ClassLib.Common.Generics.Interfaces;

namespace ClassLib.Common.Generics
{
    public class Sample
    {
        public void Write<T>(T item) where T : IName
        {
            Console.WriteLine(item.GetName());
        }
    }
}
﻿using ClassLib.Common.Generics.Interfaces;

namespace ClassLib.Common.Generics
{
    public class GenString : IGen<string>
    {
        public string Add(string x, string y)
        {
            return x + y;
        }
    }
}
﻿using ClassLib.Common.Generics.Interfaces;

namespace ClassLib.Common.Generics
{
    public class B : IName
    {
        public string GetName()
        {
            return "B";
        }
    }
}
﻿using System;

namespace ClassLib.Common.Generics
{
    public class Shape
    {
    }

    public class Circle : Shape
    {
    }

    public class Test1
    {
        public void Display(Shape shape)
        {
            throw new NotSupportedException();
        }

        public void ShowShapes()
        {
            var c = new Circle();
            Display(c);

            var sh = new Shape();
            Display(sh);
        }
    }

    public class Test2
    {
        public Shape GetShape()
        {
            return new Shape();
        }

        public Circle GetCircle()
        {
            return new Circle();
        }

        public void GetShapes()
        {
            GetShape();
            GetCircle();

            GetCircle();
        }
    }
}
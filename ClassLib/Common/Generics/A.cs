﻿using ClassLib.Common.Generics.Interfaces;

namespace ClassLib.Common.Generics
{
    public class A : IName
    {
        public string GetName()
        {
            return "A";
        }
    }
}
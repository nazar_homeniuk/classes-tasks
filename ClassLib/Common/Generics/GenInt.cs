﻿using ClassLib.Common.Generics.Interfaces;

namespace ClassLib.Common.Generics
{
    public class GenInt : IGen<int>
    {
        public int Add(int x, int y)
        {
            return x + y;
        }
    }
}
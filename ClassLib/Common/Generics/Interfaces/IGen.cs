﻿namespace ClassLib.Common.Generics.Interfaces
{
    public interface IGen<T>
    {
        T Add(T x, T y);
    }
}
﻿namespace ClassLib.Common.Generics.Interfaces
{
    public interface IName
    {
        string GetName();
    }
}
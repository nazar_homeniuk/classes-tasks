﻿using System;

namespace ClassLib.Common.Vector
{
    public class Vector
    {
        public Vector(double x = 0.0, double y = 0.0, double z = 0.0)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public double X { get; set; }

        public double Y { get; set; }

        public double Z { get; set; }

        public double Abs()
        {
            return Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2) + Math.Pow(Z, 2));
        }

        public static double MixedProd(Vector first, Vector second, Vector third)
        {
            return ScalarProd(first, CrossProd(second, third));
        }

        public static Vector CrossProd(Vector first, Vector second)
        {
            return new Vector(first.Y * second.Z - first.Z * second.Y, -1 * (first.X * second.Z - first.Z * second.X),
                first.X * second.Y - first.Y * second.X);
        }

        public static double ScalarProd(Vector first, Vector second)
        {
            return first.Abs() * second.Abs() * Cos(first, second);
        }

        public static Vector operator +(Vector first, Vector second)
        {
            return new Vector(first.X + second.X, first.Y + second.Y, first.Z + second.Z);
        }

        public static Vector operator -(Vector first, Vector second)
        {
            return new Vector(first.X - second.X, first.Y - second.Y, first.Z - second.Z);
        }

        public static bool operator <(Vector first, Vector second)
        {
            return first.Abs() < second.Abs();
        }

        public static bool operator >(Vector first, Vector second)
        {
            return first.Abs() > second.Abs();
        }

        public static bool operator ==(Vector first, Vector second)
        {
            return first != null && first.Abs() == second.Abs();
        }

        public static bool operator !=(Vector first, Vector second)
        {
            return first != null && first.Abs() != second.Abs();
        }

        public static double Cos(Vector first, Vector second)
        {
            return ScalarProd(first, second) / (first.Abs() * second.Abs());
        }

        public static double Sin(Vector first, Vector second)
        {
            return 1 - Math.Pow(ScalarProd(first, second) / (first.Abs() * second.Abs()), 2);
        }

        public override string ToString()
        {
            return $"X: {X} Y: {Y} Z: {Z}";
        }
    }
}
﻿using System;

namespace ClassLib.Common.Vector
{
    public static class VectorCreator
    {
        public static Vector CreateVector()
        {
            double x;
            double y;
            double z;
            Console.Clear();
            Console.Write("X: ");
            while (!double.TryParse(Console.ReadLine(), out x)) Console.Write("X: ");

            Console.Write("Y: ");
            while (!double.TryParse(Console.ReadLine(), out y)) Console.Write("Y: ");

            Console.Write("Z: ");
            while (!double.TryParse(Console.ReadLine(), out z)) Console.Write("Z: ");

            return new Vector(x, y, z);
        }
    }
}
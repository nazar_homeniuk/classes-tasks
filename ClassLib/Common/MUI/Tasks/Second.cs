﻿using System;
using System.Globalization;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.MUI.Tasks
{
    /// <summary>
    ///     Given names of two students. Compare these two tapes, regardless of regional settings.
    ///     Determine which of these two bands is longer.
    ///     Is it true that the first and last characters of each of these are the same?
    /// </summary>
    public class Second : IRunnable
    {
        public void Run()
        {
            string first;
            string second;
            string culture;
            Console.Write("2. Enter the first string: ");
            first = Console.ReadLine();
            Console.Write("Enter the second string: ");
            second = Console.ReadLine();
            Console.Write("Enter the culture info: ");
            culture = Console.ReadLine();
            if (string.Compare(first, second, true, new CultureInfo(culture)) == 1)
                Console.WriteLine($"{first} > {second}");
            else if (string.Compare(first, second, true, new CultureInfo(culture)) == -1)
                Console.WriteLine($"{first} < {second}");
            else
                Console.WriteLine($"{first} == {second}");
            Console.WriteLine($"{first} length = {first.Length}\n{second} length = {second.Length}");
            Console.WriteLine(first[0] == second[0] && first[first.Length - 1] == second[second.Length - 1]);
        }
    }
}
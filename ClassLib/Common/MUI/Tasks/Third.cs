﻿using System;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.MUI.Tasks
{
    /// <summary>
    ///     In the given word replace all occurrences of the letter 'a' to 'u'.
    /// </summary>
    public class Third : IRunnable
    {
        public void Run()
        {
            Console.Write("3. Enter the string: ");
            var str2 = Console.ReadLine();
            str2 = str2.Replace('a', 'u');
            Console.WriteLine("Result: {0}", str2);
        }
    }
}
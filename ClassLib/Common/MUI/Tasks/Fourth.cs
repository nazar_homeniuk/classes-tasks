﻿using System;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.MUI.Tasks
{
    /// <summary>
    ///     Read the string from the console (the words are separated by commas and spaces, for example: "one, two, three
    ///     ...").
    ///     Sort all words in this tape alphabetically.
    /// </summary>
    public class Fourth : IRunnable
    {
        public void Run()
        {
            Console.Write("4. Enter the string: ");
            var str2 = Console.ReadLine();
            var strArr = str2.Split(", ".ToCharArray());
            System.Array.Sort(strArr);
            foreach (var i in strArr) Console.WriteLine(i);
        }
    }
}
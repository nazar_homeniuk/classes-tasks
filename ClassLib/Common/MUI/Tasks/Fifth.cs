﻿using System;
using System.Globalization;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.MUI.Tasks
{
    /// <summary>
    ///     Read the culture id from the console (for example: "en-GB", "en-US", "uk-UA", "ar-IQ" ...).
    ///     Output the current date in a format that is specific to the given culture.
    /// </summary>
    public class Fifth : IRunnable
    {
        public void Run()
        {
            Console.Write("5. Enter the culture info: ");
            var culture = Console.ReadLine();
            Console.WriteLine(DateTime.Now.ToString(new CultureInfo(culture)));
        }
    }
}
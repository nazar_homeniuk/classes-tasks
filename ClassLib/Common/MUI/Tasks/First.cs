﻿using System;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.MUI.Tasks
{
    /// <summary>
    ///     A string containing a set of user names separated by commas is given to receive the names of these users in
    ///     lowercase.
    /// </summary>
    public class First : IRunnable
    {
        public void Run()
        {
            Console.WriteLine("1. Enter the users:");
            var str = Console.ReadLine();
            var result = str.Split(',');
            foreach (var i in result) Console.WriteLine(i.ToLower());
        }
    }
}
﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ClassLib.Common.Serialization
{
    public class CarBinarySerializer
    {
        private readonly List<Car> _cars;

        public CarBinarySerializer(List<Car> cars)
        {
            _cars = cars;
        }

        public void Serialize(string path)
        {
            var serializer = new BinaryFormatter();
            using (var stream = new FileStream(path, FileMode.OpenOrCreate))
            {
                serializer.Serialize(stream, _cars);
            }
        }
    }
}
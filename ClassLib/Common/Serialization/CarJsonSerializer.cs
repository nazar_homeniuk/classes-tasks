﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;

namespace ClassLib.Common.Serialization
{
    public class CarJsonSerializer
    {
        private readonly List<Car> _cars;

        public CarJsonSerializer(List<Car> cars)
        {
            _cars = cars;
        }

        public void Serialize(string path)
        {
            var serializer = new DataContractJsonSerializer(typeof(List<Car>));
            using (var stream = new FileStream(path, FileMode.OpenOrCreate))
            {
                serializer.WriteObject(stream, _cars);
            }
        }
    }
}
﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace ClassLib.Common.Serialization
{
    public class CarXmlSerializer
    {
        private readonly List<Car> _cars;

        public CarXmlSerializer(List<Car> cars)
        {
            _cars = cars;
        }

        public void Serialize(string path)
        {
            var serializer = new XmlSerializer(typeof(List<Car>));
            using (var stream = new FileStream(path, FileMode.OpenOrCreate))
            {
                serializer.Serialize(stream, _cars);
            }
        }
    }
}
﻿using System;
using System.Runtime.Serialization;

namespace ClassLib.Common.Serialization
{
    [Serializable]
    [DataContract]
    public class Car
    {
        [DataMember] public string Name { get; set; }

        [DataMember] public decimal Price { get; set; }

        [DataMember] public int Year { get; set; }
    }
}
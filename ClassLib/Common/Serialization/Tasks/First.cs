﻿using System;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.Serialization.Tasks
{
    /// <summary>
    ///     Serialize and deserialize a collection of instances of the Car (Binary / XML) instance.
    /// </summary>
    public class First : IRunnable
    {
        public void Run()
        {
            Console.WriteLine("Enter the cars");
            var cars = CarReader.ReadCars();
            Console.WriteLine("Enter the file path: ");
            string path = Console.ReadLine();
            var bin = new CarBinarySerializer(cars);
            bin.Serialize(path);
            var xml = new CarXmlSerializer(cars);
            xml.Serialize(path);
            var json = new CarJsonSerializer(cars);
            json.Serialize(path);
            Console.WriteLine("Success!");
            Console.ReadKey();
        }
    }
}
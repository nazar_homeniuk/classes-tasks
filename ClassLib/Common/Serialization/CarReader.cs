﻿using System;
using System.Collections.Generic;

namespace ClassLib.Common.Serialization
{
    public static class CarReader
    {
        public static List<Car> ReadCars()
        {
            var cars = new List<Car>();
            while (true)
            {
                Console.Write("Name: ");
                var name = Console.ReadLine();
                Console.Write("Price: ");
                var price = decimal.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                Console.Write("Year: ");
                var year = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                cars.Add(new Car {Name = name, Price = price, Year = year});
                Console.WriteLine("Press [enter] to serialize, any key to add new car");
                var key = Console.ReadKey().Key;
                if (key == ConsoleKey.Enter) break;
            }

            return cars;
        }
    }
}
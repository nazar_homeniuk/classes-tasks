﻿using System;
using System.Linq;
using System.Threading;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.Threads.Tasks
{
    /// <summary>
    ///     Use synchronization mechanisms to calculate the sum of paired elements of an array of numbers using Threads.
    /// </summary>
    public class Second : IRunnable
    {
        private readonly int[] _array;

        private readonly object _lockObj = new object();

        private readonly int[] _result;

        public Second()
        {
            _array = GenerateArray(10000000);
            _result = new int[10];
        }

        public void Run()
        {
            for (var i = 0; i < 10; ++i)
            {
                var thread = new Thread(Sum);
                thread.Start(new[] {i * 1000, i * 1000 + 1000, i});
            }

            Console.WriteLine(_result.Sum());
        }

        private int[] GenerateArray(int size)
        {
            var array = new int[size];
            var rand = new Random();
            for (var i = 0; i < array.Length; ++i) array[i] = rand.Next(0, 10000);

            return array;
        }

        private void Sum(object obj)
        {
            lock (_lockObj)
            {
                var arr = (int[]) obj;
                for (var i = arr[0]; i < arr[1]; ++i)
                    if (_array[i] % 2 == 0)
                        _result[arr[2]] += _array[i];
            }
        }
    }
}
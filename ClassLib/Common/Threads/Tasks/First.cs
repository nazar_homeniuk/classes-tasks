﻿using System;
using System.Linq;
using System.Threading;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.Threads.Tasks
{
    /// <summary>
    ///     Do not use synchronization mechanisms to calculate the sum of paired elements of an array of numbers using Threads.
    /// </summary>
    public class First : IRunnable
    {
        private readonly int[] _array;

        private readonly int[] _result;

        public First()
        {
            _array = GenerateArray(1000000);
            _result = new int[10];
        }

        public void Run()
        {
            for (var i = 0; i < 10; ++i)
            {
                var thread = new Thread(Sum);
                thread.Start(new[] {i * 1000, i * 1000 + 1000, i});
            }

            Console.WriteLine(_result.Sum());
        }


        private int[] GenerateArray(int size)
        {
            var resArray = new int [size];
            var rand = new Random();
            for (var i = 0; i < resArray.Length; ++i) resArray[i] = rand.Next(int.MinValue, int.MaxValue);

            return resArray;
        }

        private void Sum(object obj)
        {
            var arr = (int[]) obj;
            for (var i = arr[0]; i < arr[1]; ++i)
                if (_array[i] % 2 == 0)
                    _result[arr[2]] += _array[i];
        }
    }
}
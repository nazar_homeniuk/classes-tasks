﻿using System;
using System.Reflection;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.Reflection.Tasks
{
    /// <summary>
    ///     Create a DLL class library and display information about the assembly and all its components (classes, methods, and
    ///     (classes, methods, and (classes, methods, etc.).
    /// </summary>
    public class First : IRunnable
    {
        public void Run()
        {
            Console.WriteLine("Enter the assembly file path: ");
            string path = Console.ReadLine();
            var assembly =
                Assembly.Load(path ?? throw new InvalidOperationException());
            Console.WriteLine($"Assembly info\nName: {assembly.GetName()}\n");
            Console.WriteLine("***Types***");
            foreach (var type in assembly.GetTypes())
            {
                Console.WriteLine($"Type name: {type.Name}");
                Console.WriteLine("CONSTRUCTORS");
                foreach (var constructorInfo in type.GetConstructors())
                {
                    Console.WriteLine("Constructor parameters");
                    foreach (var parameterInfo in constructorInfo.GetParameters())
                        Console.WriteLine($"Name: {parameterInfo.Name}\nType: {parameterInfo.ParameterType}");
                }

                Console.WriteLine("FIELDS");
                foreach (var fieldInfo in type.GetFields())
                    Console.WriteLine($"Name: {fieldInfo.Name}\nType: {fieldInfo.FieldType}");
                Console.WriteLine("METHODS");
                foreach (var methodInfo in type.GetMethods())
                {
                    Console.WriteLine($"Name: {methodInfo.Name}\nReturn type: {methodInfo.ReturnType}");
                    Console.WriteLine("Method parameters");
                    foreach (var parameterInfo in methodInfo.GetParameters())
                        Console.WriteLine($"Name: {parameterInfo.Name}\nType: {parameterInfo.ParameterType}");
                }
            }
        }
    }
}
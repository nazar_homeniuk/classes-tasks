﻿using System;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.Array
{
    public class ConsoleReader : IReader
    {
        public string ReadText()
        {
            return Console.ReadLine();
        }
    }
}
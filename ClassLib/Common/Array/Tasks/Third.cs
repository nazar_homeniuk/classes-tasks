﻿using ClassLib.Core.Interfaces;

namespace ClassLib.Common.Array.Tasks
{
    /// <summary>
    ///     Given an array of integers, check it for symmetry (for example, the following arrays are symmetric.
    /// </summary>
    public class Third : IRunnable
    {
        public Third(IReader reader, IPrinter printer)
        {
            _reader = reader;
            _printer = printer;
        }

        private IReader _reader { get; }

        private IPrinter _printer { get; }

        private int[] _arr { get; set; }

        public void Run()
        {
            _printer.PrintText("<---=== THIRD TASK ===--->\n");
            ArrayInit();
            ArrayInput(_arr);
            _printer.PrintText("<---=== YOUR ARRAY ===--->\n");
            PrintArray(_arr);
            _printer.PrintText("\n<---=== RESULT ===--->\n");
            _printer.PrintText(IsSymmetry() + "\n");
        }

        private void ArrayInit()
        {
            int n;
            _printer.PrintText("Enter the size of array: ");
            while (!int.TryParse(_reader.ReadText(), out n) || n == 0) _printer.PrintText("Enter the size of array: ");

            _arr = new int[n];
        }

        private bool IsSymmetry()
        {
            var result = true;
            int n;
            n = _arr.Length;
            for (var i = 0; i < n; ++i)
                if (_arr[i] != _arr[_arr.Length - i - 1])
                {
                    result = false;
                    break;
                }

            return result;
        }

        private void PrintArray(int[] arr)
        {
            for (var i = 0; i < arr.Length; ++i) _printer.PrintText(arr[i] + " ");
        }

        private void ArrayInput(int[] arr)
        {
            _printer.PrintText("Enter the elements of array:\n");
            for (var i = 0; i < arr.Length; ++i)
            {
                _printer.PrintText("arr[" + i + "]:");
                while (!int.TryParse(_reader.ReadText(), out arr[i])) _printer.PrintText("arr[" + i + "]:");
            }
        }
    }
}
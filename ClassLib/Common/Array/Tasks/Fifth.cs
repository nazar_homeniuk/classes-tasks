﻿using System.Linq;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.Array.Tasks
{
    /// <summary>
    ///     Run task 4 using jagged array.
    /// </summary>
    public class Fifth : IRunnable
    {
        public Fifth(IReader reader, IPrinter printer)
        {
            _reader = reader;
            _printer = printer;
        }

        private int[][] _arr { get; set; }

        private int[] _vector { get; set; }

        private IReader _reader { get; }

        private IPrinter _printer { get; }

        public void Run()
        {
            _printer.PrintText("<---=== FIFTH TASK ===--->\n");
            ArrayInit();
            ArrayInput(_arr);
            MaxRowsElements();
            _printer.PrintText("<---=== YOUR ARRAY ===--->\n");
            PrintArray(_arr);
            _printer.PrintText("\n<---=== RESULT VECTOR ===--->\n");
            PrintArray(_vector);
            _printer.PrintText("\n");
        }

        private void MaxRowsElements()
        {
            for (var i = 0; i < _arr.Length; ++i) _vector[i] = _arr[i].Max();
        }

        private void ArrayInit()
        {
            int n;
            int m;
            _printer.PrintText("Enter n: ");
            while (!int.TryParse(_reader.ReadText(), out n) || n <= 0) _printer.PrintText("Enter n: ");

            _printer.PrintText("Enter m: ");
            while (!int.TryParse(_reader.ReadText(), out m) || m <= 0) _printer.PrintText("Enter m: ");

            _arr = new int[n][];
            for (var i = 0; i < n; ++i) _arr[i] = new int[m];

            _vector = new int[n];
        }

        private void PrintArray(int[][] arr)
        {
            for (var i = 0; i < arr.Length; ++i)
            {
                for (var j = 0; j < arr[i].Length; ++j) _printer.PrintText(arr[i][j] + " ");

                _printer.PrintText("\n");
            }
        }

        private void PrintArray(int[] arr)
        {
            for (var i = 0; i < arr.Length; ++i) _printer.PrintText(arr[i] + " ");
        }

        private void ArrayInput(int[][] arr)
        {
            _printer.PrintText("Enter the elements of array:\n");
            for (var i = 0; i < arr.Length; ++i)
            {
                for (var j = 0; j < arr[i].Length; ++j)
                {
                    _printer.PrintText("arr[" + i + "," + j + "]:");
                    while (!int.TryParse(_reader.ReadText(), out arr[i][j]))
                        _printer.PrintText("arr[" + i + "," + j + "]:");
                }
            }
        }
    }
}
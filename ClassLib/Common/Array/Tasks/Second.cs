﻿using System;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.Array.Tasks
{
    /// <summary>
    ///     Given a one-dimensional array of integers. Output the number of prime-number elements of a given array.
    /// </summary>
    public class Second : IRunnable
    {
        public Second(IReader reader, IPrinter printer)
        {
            _reader = reader;
            _printer = printer;
        }

        private int[] _arr { get; set; }

        private IReader _reader { get; }

        private IPrinter _printer { get; }

        public void Run()
        {
            _printer.PrintText("<---=== SECOND TASK ===--->\n");
            ArrayInit();
            ArrayInput(_arr);
            _printer.PrintText("<---=== YOUR ARRAY ===--->\n");
            PrintArray(_arr);
            _printer.PrintText("\n<---=== SIMPLE NUMBERS ===--->\n");
            PrintSimpleNumbers(_arr);
            _printer.PrintText("\n");
        }

        public void ArrayInput(int[] arr)
        {
            _printer.PrintText("Enter the elements of array:\n");
            for (var i = 0; i < arr.Length; ++i)
            {
                _printer.PrintText("arr[" + i + "]:");
                while (!int.TryParse(_reader.ReadText(), out arr[i])) _printer.PrintText("arr[" + i + "]:");
            }
        }

        private void PrintSimpleNumbers(int[] arr)
        {
            bool isSimple;
            for (var i = 0; i < arr.Length; ++i)
            {
                isSimple = true;
                if (arr[i] == 1) continue;

                for (var j = 2; j < arr[i]; ++j)
                    if (arr[i] % j == 0)
                    {
                        isSimple = false;
                        break;
                    }

                if (isSimple) _printer.PrintText(arr[i] + " ");
            }
        }

        private void ArrayInit()
        {
            int n;
            _printer.PrintText("Enter the size of array: ");
            while (!int.TryParse(Console.ReadLine(), out n) || n == 0) _printer.PrintText("Enter the size of array: ");

            _arr = new int[n];
        }

        private void PrintArray(int[] arr)
        {
            for (var i = 0; i < arr.Length; ++i) _printer.PrintText(arr[i] + " ");
        }
    }
}
﻿using ClassLib.Core.Interfaces;

namespace ClassLib.Common.Array.Tasks
{
    /// <summary>
    ///     A two-dimensional array of integers (m x n, m> 1, n> 1) is given. On the basis of a given array, form a vector that
    ///     consists of elements that are maximal in each row of a given array.
    /// </summary>
    public class Fourth : IRunnable
    {
        public Fourth(IReader reader, IPrinter printer)
        {
            _reader = reader;
            _printer = printer;
        }

        private int[,] _arr { get; set; }

        private int[] _vector { get; set; }

        private IReader _reader { get; }

        private IPrinter _printer { get; }

        public void Run()
        {
            _printer.PrintText("<---=== FOURTH TASK ===--->\n");
            ArrayInit();
            ArrayInput(_arr);
            MaxRowsElements();
            _printer.PrintText("<---=== YOUR ARRAY ===--->\n");
            PrintArray(_arr);
            _printer.PrintText("\n<---=== RESULT VECTOR ===--->\n");
            PrintArray(_vector);
            _printer.PrintText("\n");
        }

        private void MaxRowsElements()
        {
            int max;
            for (var i = 0; i < _arr.GetLength(0); ++i)
            {
                max = _arr[i, 0];
                for (var j = 0; j < _arr.GetLength(1); ++j)
                    if (_arr[i, j] > max)
                        max = _arr[i, j];

                _vector[i] = max;
            }
        }

        private void ArrayInit()
        {
            int n;
            int m;
            _printer.PrintText("Enter n: ");
            while (!int.TryParse(_reader.ReadText(), out n) || n <= 0) _printer.PrintText("Enter n: ");

            _printer.PrintText("Enter m: ");
            while (!int.TryParse(_reader.ReadText(), out m) || m <= 0) _printer.PrintText("Enter m: ");

            _arr = new int[n, m];
            _vector = new int[n];
        }

        private void PrintArray(int[,] arr)
        {
            for (var i = 0; i < arr.GetLength(0); ++i)
            {
                for (var j = 0; j < arr.GetLength(1); ++j) _printer.PrintText(arr[i, j] + " ");

                _printer.PrintText("\n");
            }
        }

        private void PrintArray(int[] arr)
        {
            for (var i = 0; i < arr.Length; ++i) _printer.PrintText(arr[i] + " ");
        }

        private void ArrayInput(int[,] arr)
        {
            _printer.PrintText("Enter the elements of array:\n");
            for (var i = 0; i < arr.GetLength(0); ++i)
            {
                for (var j = 0; j < arr.GetLength(1); ++j)
                {
                    _printer.PrintText("arr[" + i + "," + j + "]:");
                    while (!int.TryParse(_reader.ReadText(), out arr[i, j]))
                        _printer.PrintText("arr[" + i + "," + j + "]:");
                }
            }
        }
    }
}
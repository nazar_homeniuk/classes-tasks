﻿using System;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.Array
{
    public class ConsolePrinter : IPrinter
    {
        public void PrintText(string text)
        {
            Console.Write(text);
        }
    }
}
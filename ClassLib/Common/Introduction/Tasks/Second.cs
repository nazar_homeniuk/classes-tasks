﻿using System;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.Introduction.Tasks
{
    public class Second : IRunnable
    {
        public void Run()
        {
            Console.WriteLine("<----===== SECOND TASK =====---->");
            Console.Write("1. Enter names of users: ");
            var str = Console.ReadLine();
            if (str != null)
            {
                var strArr = str.Split(',');
                foreach (var i in strArr) Console.WriteLine(i.ToLower());
                Console.Write("2. Enter the first string: ");
                var first = Console.ReadLine();
                Console.Write("Enter the second string: ");
                var second = Console.ReadLine();
                if (first != null && String.Compare(first, second, StringComparison.Ordinal) == 1)
                    Console.WriteLine($"{first} > {second}");
                else if (first != null && String.Compare(first, second, StringComparison.Ordinal) == -1)
                    Console.WriteLine($"{first} < {second}");
                else
                    Console.WriteLine($"{first} == {second}");
                if (first != null && second != null)
                {
                    Console.WriteLine($"{first} length = {first.Length}\n{second} length = {second.Length}");
                    Console.WriteLine(first[0] == second[0] &&
                                      first[first.Length - 1] == second[second.Length - 1]);
                }

                Console.Write("3. Enter the string: ");
                var str2 = Console.ReadLine();
                if (str2 != null)
                {
                    str2 = str2.Replace('a', 'u');
                    Console.WriteLine("Result: {0}", str2);
                    Console.Write("4. Enter the string: ");
                }

                str2 = Console.ReadLine();
                if (str2 != null) strArr = str2.Split(", ".ToCharArray());
                System.Array.Sort(strArr);
                foreach (var i in strArr) Console.WriteLine(i);
            }

        }
    }
}

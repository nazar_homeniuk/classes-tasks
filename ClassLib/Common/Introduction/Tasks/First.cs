﻿using System;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.Introduction.Tasks
{
    public class First : IRunnable
    {
        public void Run()
        {
            Console.WriteLine("<----===== FIRST TASK =====---->");
            Console.Write("1. Enter the radius: ");
            var r = Convert.ToDouble(Console.ReadLine());
            var s = Math.PI * r * r;
            Console.WriteLine("Result: {0}", s);
            double a;
            Console.Write("2. Enter the side of the cube: ");
            a = Convert.ToDouble(Console.ReadLine());
            var volume = a * a * a;
            Console.WriteLine("Result: {0}", volume);
            double m;
            double n;
            while (true)
            {
                Console.Write("3. Enter n: ");
                m = Convert.ToDouble(Console.ReadLine());
                if (m <= 1 || m >= 5) continue;
                Console.Write("Enter m: ");
                n = Convert.ToDouble(Console.ReadLine());
                if (n <= 1 || n >= 5) continue;
                break;
            }

            var result = Math.Pow(m, n);
            Console.WriteLine("Result: {0}", result);
            Console.Write("4. Enter the number: ");
            result = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Result: {0}", Math.Truncate(result));
            while (true)
            {
                Console.Write("5. Enter n: ");
                n = Convert.ToInt32(Console.ReadLine());
                if (n <= 2) continue;
                break;
            }

            result = n / 2;
            Console.WriteLine("Result: {0}", Math.Round(result, 2));
            double d;
            while (true)
            {
                Console.Write("6. Enter d: ");
                d = Convert.ToDouble(Console.ReadLine());
                if (d <= 0) continue;
                break;
            }

            result = Math.Floor(d) + 1;
            Console.WriteLine("Result: {0}", result * result);
        }
    }
}

﻿using System;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.Exceptions.Tasks
{
    /// <summary>
    ///     Create a void DoSomeMath method (int a, int b) that will generate exceptions:
    ///     ArgumentException ("Parameter should be greater than 0", "a") if a 0 ArgumentException ("Parameter should be less
    ///     than 0", "b ") if b 0.
    ///     Process the generated exceptions by filtering the parameter.
    ///     Display the e.Message field on the console for each of the generated exceptions. Make sure that both handlers work
    ///     and output the corresponding message on the console.
    /// </summary>
    public class Second : IRunnable
    {
        public void Run()
        {
            int[] arr = {0};
            try
            {
                Console.WriteLine(arr[1]);
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
﻿using System;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.Exceptions.Tasks
{
    public class Fifth : IRunnable
    {
        public void Run()
        {
            try
            {
                DoSomeMath(-5, -5);
                DoSomeMath(5, 5);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void DoSomeMath(int a, int b)
        {
            if (a < 0) throw new ArgumentException("Parameter should be greater than 0", nameof(a));

            if (b > 0) throw new ArgumentException("Parameter should be less than 0", nameof(b));
        }
    }
}
﻿using System;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.Exceptions.Tasks
{
    /// <summary>
    ///     Create a recursive method. Get StackOverflowException Generation.
    ///     Create a method using one-dimensional array to get the IndexOutOfRangeException generation.
    ///     Make sure the generated exceptions are locked in the Event Viewer.
    ///     Collapse methods in tasks 1 and 2 in the try-catch-catch design.Display the e.Message field on the console for each
    ///     of the generated exceptions.
    /// </summary>
    public class First : IRunnable
    {
        public void Run()
        {
            try
            {
                //Code smell
            }
            catch (StackOverflowException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
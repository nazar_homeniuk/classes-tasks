﻿using System.Drawing;
using ClassLib.Common.StyleCoding.Interfaces;

namespace ClassLib.Common.StyleCoding
{
    public class Circle : IMoveble, ISizeble
    {
        private Point _center;

        public Circle(int radius, int centerX, int centerY)
        {
            Radius = radius;
            _center = new Point(centerX, centerY);
        }

        public int Radius { get; set; }

        public Point Center => _center;

        public void MoveLeft()
        {
            _center.X--;
        }

        public void MoveRight()
        {
            _center.X++;
        }

        public void MoveUp()
        {
            _center.Y++;
        }

        public void MoveDown()
        {
            _center.Y--;
        }

        public void ZoomIn()
        {
            Radius++;
        }

        public void ZoomOut()
        {
            Radius--;
        }
    }
}
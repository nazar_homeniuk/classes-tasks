﻿using System;
using System.Drawing;
using ClassLib.Common.StyleCoding.Interfaces;

namespace ClassLib.Common.StyleCoding
{
    public class Rect : IShape, ISizeble, IMoveble
    {
        private Point _bottomRight;
        private Point _topLeft;

        public Rect(Point topeft, Point bottomRight)
        {
            TopLeft = _bottomRight;
            BottomRight = _topLeft;
        }

        public Rect(int leftX, int leftY, int rightX, int rightY)
        {
            TopLeft = new Point(leftX, leftY);
            BottomRight = new Point(rightX, rightY);
        }

        public Point TopLeft
        {
            get => _topLeft;
            set => _topLeft = value;
        }

        public Point BottomRight
        {
            get => _bottomRight;
            set => _bottomRight = value;
        }

        public int GetHeight => Math.Abs(TopLeft.Y - BottomRight.Y);

        public int GetWidth => Math.Abs(TopLeft.X - BottomRight.X);

        public void MoveRight()
        {
            _bottomRight.X++;
            _topLeft.X++;
        }

        public void MoveLeft()
        {
            _bottomRight.X--;
            _topLeft.X--;
        }

        public void MoveUp()
        {
            _bottomRight.Y++;
            _topLeft.Y++;
        }

        public void MoveDown()
        {
            _bottomRight.Y--;
            _topLeft.Y--;
        }

        public void ZoomIn()
        {
            _bottomRight.Y--;
            _topLeft.Y++;
            _bottomRight.X++;
            _topLeft.X--;
        }

        public void ZoomOut()
        {
            _bottomRight.Y++;
            _topLeft.Y--;
            _bottomRight.X--;
            _topLeft.X++;
        }

        public static Rect Intersect(Rect first, Rect second)
        {
            var x1 = Math.Max(first.TopLeft.X, second.TopLeft.X);
            var x2 = Math.Min(first.TopLeft.X + first.GetWidth, second.TopLeft.X + first.GetWidth);
            var y1 = Math.Min(first.TopLeft.Y, second.TopLeft.Y);
            var y2 = Math.Max(first.BottomRight.Y, second.BottomRight.Y);
            if (x2 >= x1 && y1 >= y2) return new Rect(x1, y1, x2, y2);
            return null;
        }

        public static Rect Union(Rect first, Rect second)
        {
            var x1 = Math.Min(first.TopLeft.X, second.TopLeft.X);
            var x2 = Math.Max(first.TopLeft.X + first.GetWidth, second.TopLeft.X + first.GetWidth);
            var y1 = Math.Max(first.TopLeft.Y, second.TopLeft.Y);
            var y2 = Math.Min(first.BottomRight.Y, second.BottomRight.Y);
            return new Rect(x1, y1, x2, y2);
        }
    }
}
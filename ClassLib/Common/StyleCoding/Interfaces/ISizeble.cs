﻿namespace ClassLib.Common.StyleCoding.Interfaces
{
    public interface ISizeble
    {
        void ZoomIn();
        void ZoomOut();
    }
}
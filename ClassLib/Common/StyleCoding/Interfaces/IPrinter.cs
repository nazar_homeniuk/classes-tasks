﻿namespace ClassLib.Common.StyleCoding.Interfaces
{
    public interface IPrinter
    {
        void Print(bool[,] arr);
    }
}
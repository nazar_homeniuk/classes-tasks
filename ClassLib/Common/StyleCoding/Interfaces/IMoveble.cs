﻿namespace ClassLib.Common.StyleCoding.Interfaces
{
    public interface IMoveble
    {
        void MoveLeft();
        void MoveRight();
        void MoveUp();
        void MoveDown();
    }
}
﻿namespace ClassLib.Common.StyleCoding.Interfaces
{
    public interface IPrintable
    {
        void Print();
    }
}
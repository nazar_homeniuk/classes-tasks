﻿using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace ClassLib.Common.InputOutput.Classes
{
    public class FileSearcher
    {
        public void Search(DirectoryInfo directory, ICollection<FileInfo> files, string fileName)
        {
            foreach (var dir in directory.GetDirectories())
            {
                foreach (var file in dir.GetFiles())
                {
                    var regex = new Regex($@"{fileName}(\w*)");
                    var matches = regex.Matches(file.Name);
                    if (matches.Count >= 1 && file.Extension == ".txt") files.Add(file);
                }

                Search(dir, files, fileName);
            }
        }
    }
}
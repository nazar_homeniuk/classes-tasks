﻿using System;
using System.IO;
using System.Text;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.InputOutput
{
    /// <summary>
    ///     Display the contents of the specified directory as a list of files, sub directories containing the contents of
    ///     these directories.
    /// </summary>
    public class First : IRunnable
    {
        private StringBuilder _pad;

        public void Run()
        {
            Console.Write("Enter the directory path: ");
            var path = Console.ReadLine();
            Console.WriteLine("Directory info by path: {0}", path);
            ShowDirectories(new DirectoryInfo(path ?? throw new InvalidOperationException()));
            _pad = new StringBuilder();
        }

        private void ShowDirectories(DirectoryInfo info)
        {
            var directories = info.GetDirectories();
            var files = info.GetFiles();
            foreach (var i in directories)
            {
                Console.WriteLine(_pad + "<---=== DIRECTORY ===--->");
                Console.WriteLine(_pad + "Name: {0}\n", i.Name);
                foreach (var j in files)
                {
                    Console.WriteLine(_pad + "<---=== FILE ===--->");
                    Console.WriteLine(_pad + "Name: {0}\n" + _pad + "Size: {1} B", j.Name, j.Length);
                }

                ShowDirectories(i);
                _pad.Append(' ', 5);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using ClassLib.Common.InputOutput.Classes;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.InputOutput
{
    /// <summary>
    ///     Create an application that allows you to find a file with a .txt extension for a partially-specified name.
    /// </summary>
    public class Second : IRunnable
    {
        public void Run()
        {
            var files = new List<FileInfo>();
            Console.Write("Enter the directory path: ");
            var path = Console.ReadLine();
            Console.Write("Enter the file name: ");
            var fileName = Console.ReadLine();
            var directoryInfo = new DirectoryInfo(path);
            var fileSearcher = new FileSearcher();
            fileSearcher.Search(directoryInfo, files, fileName);
            foreach (var fileInfo in files) Console.WriteLine($"Name: {fileInfo.Name}\nSize: {fileInfo.Length}");
        }
    }
}
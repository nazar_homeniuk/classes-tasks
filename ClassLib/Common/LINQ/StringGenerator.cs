﻿using System;
using System.Text;

namespace ClassLib.Common.LINQ
{
    public static class StringGenerator
    {
        private static readonly Random Rand = new Random();

        public static string Generate(int size)
        {
            int id;
            char let;
            var result = new StringBuilder();
            for (var i = 0; i < size; ++i)
            {
                id = Rand.Next(0, 26);
                let = (char) ('a' + id);
                result.Append(let);
            }

            return result.ToString().ToUpper();
        }
    }
}
﻿using System;
using System.IO;

namespace ClassLib.Common.MemoryManagment
{
    public class FileConnector : DataConnector, IDisposable
    {
        private bool _disposedValue;

        private StreamReader _fileStream;

        public FileConnector(string connectionString) : base(connectionString)
        {
        }


        public override string Read()
        {
            return _fileStream.ReadToEnd();
        }

        public override void Open()
        {
            _fileStream = new StreamReader(ConnectionString);
        }

        protected virtual void Disposing(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _fileStream.Dispose();
                }

                _disposedValue = true;
            }
        }

        ~FileConnector()
        {
            Disposing(false);
        }

        void IDisposable.Dispose()
        {
            Disposing(true);
            GC.SuppressFinalize(this);
        }
    }
}
﻿namespace ClassLib.Common.MemoryManagment
{
    public class DataConnector
    {
        public DataConnector(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public string ConnectionString { get; }

        public virtual void Open()
        {
        }

        public virtual void Close()
        {
        }

        public virtual string Read()
        {
            return string.Empty;
        }
    }
}
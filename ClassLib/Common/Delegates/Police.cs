﻿using System;

namespace ClassLib.Common.Delegates
{
    public class Police
    {
        public void Subscribe(object sender, HouseEventArgs e)
        {
            Console.WriteLine("The policeman on his way!");
        }
    }
}
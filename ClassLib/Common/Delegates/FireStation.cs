﻿using System;

namespace ClassLib.Common.Delegates
{
    public class FireStation
    {
        public void Subscribe(object sender, HouseEventArgs e)
        {
            Console.WriteLine("The firefighter's on their way!");
        }
    }
}
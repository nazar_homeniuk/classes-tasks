﻿using System;

namespace ClassLib.Common.Delegates
{
    public class SmartHouse
    {
        public event EventHandler<HouseEventArgs> Fire;

        public event EventHandler<HouseEventArgs> Robbery;

        public void OnFire()
        {
            Fire?.Invoke(this, new HouseEventArgs {Event = "Fire"});
        }

        public void OnRobbery()
        {
            Robbery?.Invoke(this, new HouseEventArgs {Event = "Robbery"});
        }
    }
}
﻿using System;

namespace ClassLib.Common.Delegates
{
    public class HouseEventArgs : EventArgs
    {
        public string Event { get; set; }
    }
}
﻿using System;

namespace ClassLib.Common.Delegates
{
    public class Owner
    {
        public void Subscribe(object sender, HouseEventArgs e)
        {
            Console.WriteLine("An owner was notified about {0}", e.Event);
        }
    }
}
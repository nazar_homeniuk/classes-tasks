﻿using System;

namespace ClassLib.Common.Delegates
{
    public class Plane
    {
        private readonly int _duration;
        private readonly string _name;
        private readonly DateTime _start;

        public Plane(int duration, string name)
        {
            _duration = duration;
            _name = name;
            _start = DateTime.Now;
        }

        public string GetName()
        {
            return _name;
        }

        public string TakeOff()
        {
            return $"{_name} {_start}";
        }

        public string Landing()
        {
            return $"{_name} {_start.AddHours(_duration)}";
        }
    }
}
﻿using System;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.Delegates.Tasks
{
    /// <summary>
    ///     Create class Plane with and instance methods GetName() ,TakeOff() and Landing(), also Plane should have private
    ///     field _duration that is going to be set in constructor.
    ///     GetName – return especial plane name(better if you set it in constructor)
    ///     TakeOff – set private field _start to the current DateTime and return eloquent string with plane name and take off
    ///     time.
    ///     Landing – calculate landing time by adding duration time to _start value. If Start value is null, method should
    ///     throw an exception with relevant message.Return string with appropriate message about plane name and landing time.
    ///     Define delegate that doesn`t take any parameters and returns string.
    ///     In the Main method:
    ///     Create Plane instance and add all it`s methods to delegate chain in appropriate order, invoke it and write output
    ///     to console.
    ///     Repeat previous step with another Plane instance, but add Landing method before TakeOff, invoke it and write output
    ///     to console.
    ///     Rewrite both chains to invoke delegates explicitly.(Use StringBuilder to collect results of the methods)
    /// </summary>
    public class First : IRunnable
    {
        private Deleg _planeDelegate;

        public void Run()
        {
            var plane = new Plane(2, "Lviv-Prague");
            _planeDelegate += plane.TakeOff;
            _planeDelegate += plane.Landing;
            foreach (var @delegate in _planeDelegate.GetInvocationList())
            {
                var i = (Deleg) @delegate;
                Console.WriteLine(i());
            }
        }

        private delegate string Deleg();
    }
}
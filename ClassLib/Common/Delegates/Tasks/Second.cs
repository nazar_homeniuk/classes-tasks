﻿using ClassLib.Core.Interfaces;

namespace ClassLib.Common.Delegates.Tasks
{
    /// <summary>
    ///     Create following classes: SmartHouse, Police, FireStation, Owner.
    ///     SmartHouse should produce events Fire and Robbery.
    ///     Police listens to event Robbery
    ///     FireStation listens to event Fire
    ///     Owner listens to both events.
    ///     Raise both events in the Main method.
    /// </summary>
    public class Second : IRunnable
    {
        public void Run()
        {
            var smartHouse = new SmartHouse();
            var police = new Police();
            var fireStation = new FireStation();
            var owner = new Owner();
            smartHouse.Robbery += owner.Subscribe;
            smartHouse.Robbery += police.Subscribe;
            smartHouse.Fire += owner.Subscribe;
            smartHouse.Fire += fireStation.Subscribe;
            smartHouse.OnRobbery();
            smartHouse.OnFire();
        }
    }
}
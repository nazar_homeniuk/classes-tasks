﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.Collections.Tasks
{
    /// <summary>
    ///     Create a list of randomly generated elements of type string (n&gt; 100, tape length 4, all uppercase characters).
    ///     Remove all duplicate elements that begin with the 'Z' symbol, sorted in descending order (z-a).
    ///     Show on the screen the number of items in the list before and after the transformations.
    ///     Create a DisplayPage method (int pageNumber) when you call a console to display the corresponding page of the
    ///     updated list (the number of items on the page is constant: 5).
    ///     Implement the program so that the page number to be output is read from the console.
    ///     If the entered ribbon is not a number then complete the program (without using LINQ).
    /// </summary>
    public class Third : IRunnable
    {
        public Third()
        {
            Strings = new List<string>();
        }

        public List<string> Strings { get; set; }

        public void Run()
        {
            Console.Clear();
            Generate();
            Console.WriteLine("Count of elements before deleting: {0}", Strings.Count);
            Delete();
            Console.WriteLine("Count of elements after deleting: {0}", Strings.Count);
            Console.ReadKey();
            int pageNumber;
            while (true)
            {
                Console.Clear();
                Console.Write("Enter the page number ({0} pages): ", Math.Ceiling(Strings.Count / 5.0));
                if (!int.TryParse(Console.ReadLine(), out pageNumber)) return;
                DisplayPage(pageNumber);
            }
        }

        public void Generate()
        {
            var rand = new Random();
            var count = rand.Next(101, 1000);
            for (var i = 0; i < count; ++i) Strings.Add(StringGenerator.Generate(4));
        }

        public void DisplayPage(int pageNumber)
        {
            var id = pageNumber * 5 - 5;

            Console.Clear();
            Console.WriteLine("PAGE {0}", pageNumber);
            for (var i = id; i < id + 5 && i < Strings.Count; ++i) Console.WriteLine(Strings[i]);

            Console.ReadKey();
        }

        public void Delete()
        {
            Strings = Strings.Distinct().ToList();
            Strings.RemoveAll(i => i.StartsWith("Z"));
        }
    }
}
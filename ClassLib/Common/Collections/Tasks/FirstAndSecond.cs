﻿using System;
using System.Collections.Generic;
using ClassLib.Core.Interfaces;

namespace ClassLib.Common.Collections.Tasks
{
    /// <summary>
    ///     Create a list of several people (> 5). Each person must have several rooms (> 2). Display the name and age of each
    ///     created person on the console.
    ///     Using the AddRange method add two more people to the created list. Output the console numbers of all people
    ///     (without using LINQ).
    /// </summary>
    public class FirstAndSecond : IRunnable
    {
        public FirstAndSecond()
        {
            Persons = new List<Person>();
        }

        public List<Person> Persons { get; set; }

        public void Run()
        {
            while (true)
            {
                Console.Clear();
                AddPeoples();
                Console.Clear();
                PrintPeolpes();
                AddRange();
                string choose;
                while (true)
                {
                    Console.WriteLine("Do you want to run this task again? (y-yes, n-no)");
                    choose = Console.ReadLine()?.ToLower();
                    if (choose == "y") break;

                    if (choose == "n") return;
                }
            }
        }

        public Person CreatePerson()
        {
            string name;
            int age;
            string number;
            List<string> numbers;
            numbers = new List<string>();
            Console.Clear();
            Console.Write("Name: ");
            name = Console.ReadLine();
            Console.Write("Age: ");
            age = Convert.ToInt32(Console.ReadLine());
            while (true)
            {
                Console.Write("Number: ");
                number = Console.ReadLine();
                if (number == string.Empty) break;

                numbers.Add(number);
            }

            return new Person {Name = name, Age = age, PhoneNumbers = numbers};
        }

        public void AddPeoples()
        {
            while (true)
            {
                Persons.Add(CreatePerson());
                Console.WriteLine("Do you want to add new person? (Y-yes, N-no)");
                if (Console.ReadLine()?.ToLower() == "n") break;
            }
        }

        public void PrintPeolpes()
        {
            Console.Clear();
            foreach (var i in Persons)
            {
                Console.WriteLine("Name: {0}\nAge: {1}\n", i.Name, i.Age);
                Console.WriteLine("***PHONE NUMBERS***");
                foreach (var j in i.PhoneNumbers) Console.WriteLine(j);
            }
        }

        public void AddRange()
        {
            Console.Clear();
            Console.WriteLine("ADD RANGE TEST");
            var persons = new List<Person>();
            for (var i = 0; i < 2; ++i) persons.Add(CreatePerson());
            Persons.AddRange(persons);
            PrintPeolpes();
        }
    }
}
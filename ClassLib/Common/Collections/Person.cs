﻿using System.Collections.Generic;

namespace ClassLib.Common.Collections
{
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public IEnumerable<string> PhoneNumbers { get; set; }
    }
}
﻿using System;
using ClassLib.Common.InputOutput;
using ClassLib.Core.Interfaces;

namespace InputOutput
{
    internal static class Program
    {
        private static void Main()
        {
            IRunnable[] tasks = {new First(), new Second()};
            foreach (var i in tasks) i.Run();

            Console.ReadKey();
        }
    }
}
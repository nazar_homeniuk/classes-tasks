﻿using System;
using System.Collections.Generic;
using ClassLib.Common.Generics;
using ClassLib.Common.Generics.Interfaces;

namespace Generics
{
    internal static class Program
    {
        private static void Main()
        {
            var arr = new List<IName>();
            arr.Add(new A());
            arr.Add(new B());

            arr.ForEach(j => j.GetName());
            Console.ReadKey();
            var i = new GenInt();
            var s = new GenString();
            Console.WriteLine(i.Add(10, 20));
            Console.WriteLine(s.Add("Hello ", "World"));
            Console.ReadKey();
            var sp = new Sample();
            sp.Write(new A());
            sp.Write(new B());
            Console.ReadKey();
        }
    }
}
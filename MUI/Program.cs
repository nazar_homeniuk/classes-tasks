﻿using System;
using ClassLib.Common.MUI.Tasks;
using ClassLib.Core.Interfaces;

namespace MUI
{
    internal static class Program
    {
        private static void Main()
        {
            IRunnable[] tasks = {new First(), new Second(), new Third(), new Fourth(), new Fifth()};
            foreach (var i in tasks)
            {
                Console.Clear();
                i.Run();
                Console.ReadKey();
            }
        }
    }
}
﻿using System;
using ClassLib.Common.Threads.Tasks;

namespace Threads
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var first = new Second();
            first.Run();
            Console.ReadKey();
        }
    }
}
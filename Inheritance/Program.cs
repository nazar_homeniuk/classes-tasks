﻿using System;
using System.Diagnostics;
using Inheritance.Interfaces;
using Inheritance.Tasks;

namespace Inheritance
{
    internal static class Program
    {
        private static void Main()
        {
            //Class example
            Console.WriteLine("***Class examples***");
            var cat = new Cat();
            var cusstomer = new Customer();
            var developer = new Developer();
            var stopwatch = new Stopwatch();
            Console.WriteLine("Cat is happy: {0}\nCusstomer is happy: {1}\nDeveloper is happy: {2}\n", cat.IsHappy,
                cusstomer.IsHappy, developer.IsHappy);
            MakeAllHappy(cat, cusstomer, developer);
            Console.WriteLine("Cat is happy: {0}\nCusstomer is happy: {1}\nDeveloper is happy: {2}\n", cat.IsHappy,
                cusstomer.IsHappy, developer.IsHappy);
            Console.ReadKey();
            var rand = new Random();
            var array = new int[1000];
            for (var i = 0; i < 1000; ++i) array[i] = rand.Next();

            var b = new BubbleSort();
            stopwatch.Start();
            PerformSort(array, b);
            stopwatch.Stop();
            Console.WriteLine("Time: {0} ms", stopwatch.ElapsedMilliseconds);
            stopwatch.Reset();
            var ob = new OptimizedBubbleSort();
            stopwatch.Start();
            PerformSort(array, ob);
            stopwatch.Stop();
            Console.WriteLine("Time: {0} ms", stopwatch.ElapsedMilliseconds);
            Console.ReadKey();
            Console.Clear();
            //Home tasks
            Console.WriteLine("***Home tasks***");
            var fig = new Figure(10.5, 10.5);
            var rect = new Rectangle(5, 5);
            var square = new Square(6, 6);
            DrawAll(fig, rect, square);
            Console.ReadKey();
        }

        public static void MakeAllHappy(params IHappiness[] args)
        {
            foreach (var item in args) item.MakeHappy();
        }

        public static void PerformSort(int[] array, ISortAlgorithm algorithm)
        {
            algorithm.Sort(array);
        }

        public static void DrawAll(params IDrawable[] args)
        {
            foreach (var i in args) i.Draw();
        }
    }
}
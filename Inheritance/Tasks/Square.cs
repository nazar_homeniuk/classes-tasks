﻿using System;

namespace Inheritance.Tasks
{
    public class Square : Figure, IDrawable
    {
        public Square(double x, double y) : base(x, y)
        {
        }

        public override void Draw()
        {
            Console.WriteLine("I am a square");
        }
    }
}
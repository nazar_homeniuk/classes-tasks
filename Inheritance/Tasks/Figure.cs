﻿using System;

namespace Inheritance.Tasks
{
    public class Figure : IDrawable
    {
        public Figure(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double X { get; }
        public double Y { get; }

        public virtual void Draw()
        {
            Console.WriteLine("I am a figure");
        }
    }
}
﻿using System;

namespace Inheritance.Tasks
{
    public class Rectangle : Figure, IDrawable
    {
        public Rectangle(double x, double y) : base(x, y)
        {
        }

        public override void Draw()
        {
            Console.WriteLine("I am a rectangle");
        }
    }
}
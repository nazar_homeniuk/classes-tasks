﻿namespace Inheritance.Tasks
{
    public interface IDrawable
    {
        void Draw();
    }
}
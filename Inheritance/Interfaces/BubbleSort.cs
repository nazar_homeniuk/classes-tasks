﻿using System;

namespace Inheritance.Interfaces
{
    public class BubbleSort : ISortAlgorithm
    {
        public void Sort(int[] array)
        {
            Console.WriteLine("BubbleSort:");
            for (var i = 0; i < array.Length; i++)
            {
                for (var j = 0; j < array.Length - 1; j++)
                    if (array[j] > array[j + 1])
                    {
                        var temp = array[j + 1];
                        array[j + 1] = array[j];
                        array[j] = temp;
                    }
            }
        }
    }
}
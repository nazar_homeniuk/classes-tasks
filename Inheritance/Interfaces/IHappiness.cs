﻿namespace Inheritance.Interfaces
{
    public interface IHappiness
    {
        bool IsHappy { get; }

        void MakeHappy();
    }
}
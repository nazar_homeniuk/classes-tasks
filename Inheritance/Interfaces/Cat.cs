﻿namespace Inheritance.Interfaces
{
    public class Cat : IHappiness
    {
        public string Status { get; set; } = "Sad";

        public bool IsHappy => Status == "Happy";

        public void MakeHappy()
        {
            Status = "Happy";
        }
    }
}
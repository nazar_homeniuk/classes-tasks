﻿namespace Inheritance.Interfaces
{
    public class Developer : IHappiness
    {
        public decimal Salary { get; set; }

        public bool IsHappy => Salary > 40000;

        public void MakeHappy()
        {
            Salary = 50000;
        }
    }
}
﻿namespace Inheritance.Interfaces
{
    public interface ISortAlgorithm
    {
        void Sort(int[] array);
    }
}
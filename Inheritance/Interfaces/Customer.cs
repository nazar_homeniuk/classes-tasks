﻿namespace Inheritance.Interfaces
{
    public class Customer : IHappiness
    {
        public bool IsHappy { get; private set; }

        public void MakeHappy()
        {
            IsHappy = true;
        }
    }
}